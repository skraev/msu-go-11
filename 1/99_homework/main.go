package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	//a := []int{1,3,4}
	a := [3]int{1, 3, 4}
	return a
}

func ReturnIntSlice() []int {
	a := []int{1, 2, 3}
	return a
}

func IntSliceToString(a []int) string {
	out := make([]string, len(a))
	for i := 0; i < len(a); i++ {
		out[i] = strconv.Itoa(a[i])
	}
	outstr := strings.Join(out, "")
	return outstr
}

func MergeSlices(a []float32, b []int32) []int {
	la := len(a)
	lb := len(b)
	c := make([]int, la+lb)
	for i := 0; i < la; i++ {
		c[i] = int(a[i])
	}
	for ii := 0; ii < lb; ii++ {
		c[ii+la] = int(b[ii])
	}
	return c
}

func GetMapValuesSortedByKey(m map[int]string) []string {
	key := make([]int, 0, len(m))
	p := 0
	for k, _ := range m {
		key = append(key, k)
		p++
	}
	sort.Ints(key)
str:=make([]string,0,len(m))
for i:=0;i<len(m);i++ {
str=append(str,m[key[i]])
}

	return str
}

func main() {
	m := map[int]string{
		10: "Зима",
		30: "Лето",
		20: "Весна",
		40: "Осень",
	}
	fmt.Println(GetMapValuesSortedByKey(m))
}
